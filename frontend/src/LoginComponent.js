import React from 'react';
import UserLists from './UserLists';

export default class LoginComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state = {username: '', password: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
      }

      handleClick = () => {
          console.log("usuario ou senha incorreto");
      }
    
      handleChange(event) {
        this.setState({username: event.target.value});
      }

      handleChangePassword(event) {
        this.setState({password: event.target.value});
      }
    
      handleSubmit(event) {
        var url = process.env.REACT_APP_BASE_URL + 'api-token-auth/';
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username: this.state.username, password: this.state.password })
        };
        
        
        fetch(url, requestOptions)
        .then((response) => {
            localStorage.setItem('status', response.status);
        });

        fetch(url, requestOptions)
        .then(response => response.json())
        .then(data => {
            localStorage.setItem('token', data.token);
            this.setState({token: data.token});
        });
        event.preventDefault();
      }

      logout(){
          localStorage.removeItem('token');
          this.setState({token: null});
      }
    

      render() {
        
        var token = localStorage.getItem('token');
        var status = localStorage.getItem('status');
        
        console.log(token)

        console.log(status)
        

        if(status === '200' && token )
            return (
               
            <div>
                <UserLists />
                <button onClick={() => this.logout()}> Logout </button>
            </div>
              
              
            );
        else if (status === '400')
            return (
            
            <form onSubmit={this.handleSubmit}>
                <label>
                Usuário:
                <input type="text" value={this.state.username} onChange={this.handleChange} required/>
                Senha:
                <input type="password" value={this.state.password} onChange={this.handleChangePassword} required/>
                </label>

                <p>Usuário ou senha incorreto.</p>               

                <input type="submit" value="Submit" onClick={this.handleClick}/>
            </form>
            )
            else 
            return (
            
            <form onSubmit={this.handleSubmit}>
                <label>
                Usuário:
                <input type="text" value={this.state.username} onChange={this.handleChange} required/>
                Senha:
                <input type="password" value={this.state.password} onChange={this.handleChangePassword} required/>
                </label>
               

                <input type="submit" value="Submit" onClick={this.handleClick}/>
            </form>
            )
        
            
      }
}